﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class targetting_manager : MonoBehaviour
{
    //public GameObject label_0;
    public GameObject label_1;
    public GameObject label_2;
    public GameObject label_3;
    public GameObject sound_label_1;
    public GameObject sound_label_2;
    public GameObject sound_label_3;

    public bool label_0_trigger;
    public bool label_1_trigger;
    public bool label_2_trigger;
    public bool label_3_trigger;

    void Start()
    {
        label_1.SetActive(false);
        label_2.SetActive(false);
        label_3.SetActive(false);
    }
    // Update is called once per frame
    void Update()
    {
        if (label_1_trigger)
        {
            setactive(1);
            label_1_trigger = false;
        }
        else if (label_2_trigger)
        {
            setactive(2);
            label_2_trigger = false;
        }
        else if (label_3_trigger)
        {
            setactive(3);
            label_3_trigger = false;
        }
        else if(label_0_trigger)
        {
            setactive(0);
            label_0_trigger = false;
        }
    }
    public void setactive(int label)
    {
        label_1.SetActive(label == 1);
        label_2.SetActive(label == 2);
        label_3.SetActive(label == 3);
        sound_label_1.SetActive(label == 1);
        sound_label_2.SetActive(label == 2);
        sound_label_3.SetActive(label == 3);
    }
}
