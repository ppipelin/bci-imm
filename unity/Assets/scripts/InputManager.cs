﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    // Start is called before the first frame update
    private bool[] input;
    void Start()
    {
        input = new bool[3];   
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setInput(int index)
    {
        for(int i = 0; i < input.Length; i++)
        {
            input[i] = (index == i-1);
        }
    }
}
