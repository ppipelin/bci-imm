﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetManager : MonoBehaviour
{
    public int[] freqencies = new int[3];
    public GameObject[] targets = new GameObject[3];
    private float[] times;
    private int cpt = 0;
    // Start is called before the first frame update
    void Start()
    {
        Application.targetFrameRate = 60;
        Time.fixedDeltaTime = 1/60;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        cpt = (cpt + 1) % 60;
        for(int i = 0; i < targets.Length; i++)
        {
           targets[i].GetComponent<SpriteRenderer>().enabled = cpt != 0 && cpt % (60 / freqencies[i]) != 0 ;
        }
    }
}
