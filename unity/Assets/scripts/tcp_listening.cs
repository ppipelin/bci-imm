﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;
//using targetting_manager;

public class tcp_listening : MonoBehaviour
{
    public targetting_manager script;
    public string sequence_input = "0 2 3 1 2 1 0 3 1 2 3 0 2 0 3 1 0 3 1 2 3 0 1 2 1 3 2 0 3 2 0 1";
    private int iterator = 0;

    public  InputManager inputManager;

    #region private members 	
    private TcpClient socketConnection;
    private Thread clientReceiveThread;
    private int[] sequence;
    #endregion
       
    // Use this for initialization 	
    void Start()
    {
        Debug.Log("Click Space to re-toggle TCP Connection");
        ConnectToTcpServer();
        sequence = Array.ConvertAll(sequence_input.Split(' '), item => Convert.ToInt32(item));
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            ConnectToTcpServer();
        }
    }
    
    /// <summary> 	
    /// Setup socket connection. 	
    /// </summary> 	
    private void ConnectToTcpServer()
    {
        try
        {
            Debug.Log("Trying to connect");
            clientReceiveThread = new Thread(new ThreadStart(ListenForData));
            clientReceiveThread.IsBackground = true;
            clientReceiveThread.Start();
        }
        catch (Exception e)
        {
            Debug.Log("On client connect exception " + e);
        }
    }
    /// <summary> 	
    /// Runs in background clientReceiveThread; Listens for incomming data. 	
    /// </summary>     
    private void ListenForData()
    {
        try
        {
            socketConnection = new TcpClient("localhost", 5678);
            Byte[] bytes = new Byte[1024];
            while (true)
            {
                // Get a stream object for reading 				
                using (NetworkStream stream = socketConnection.GetStream())
                {
                    int length;
                    // Read incomming stream into byte arrary. 					
                    while ((length = stream.Read(bytes, 0, bytes.Length)) != 0)
                    {
                        var incommingData = new byte[length];
                        Array.Copy(bytes, 0, incommingData, 0, length);
                        // Convert byte array to string message. 						
                        //string serverMessage = Encoding.ASCII.GetString(incommingData);
                        string serverMessage = Convert.ToBase64String(incommingData);
                        Debug.Log(serverMessage);
                        // Start server : AYAAAAAAAAA =
                        // Seems like C4AAAAAAAAA is start experiment
                        // And DIAAAAAAAAA is stop experiment and start delay

                        // label 1 AYEAAAAAAAA=
                        // label 2 AoEAAAAAAAA=
                        // label 3 A4EAAAAAAAA=
                        if (serverMessage.Equals("C4AAAAAAAAA="))
                        {
                            int value = sequence[iterator];
                            iterator++;
                            Debug.Log(value);
  
                            if (value == 1)
                            {
                                script.label_1_trigger = true;
                            } else if(value == 2)
                            {
                                script.label_2_trigger = true;
                            }else if (value == 3)
                            {
                                script.label_3_trigger = true;
                            }
                        }
                        else if(serverMessage.Equals("AYEAAAAAAAA="))
                        {
                            inputManager.setInput(1);
                        }

                        else if(serverMessage.Equals("AoEAAAAAAAA="))
                        {
                            inputManager.setInput(2);
                        }

                        else if(serverMessage.Equals("A4EAAAAAAAA="))
                        {
                            inputManager.setInput(3);
                        }
                        ;
                    }
                }
            }
        }
        catch (SocketException socketException)
        {
            Debug.Log("Socket exception: " + socketException);
        }
    }
    /// <summary> 	
    /// Send message to server using socket connection. 	
    /// </summary> 	
    private void SendMessage()
    {
        if (socketConnection == null)
        {
            return;
        }
        try
        {
            // Get a stream object for writing. 			
            NetworkStream stream = socketConnection.GetStream();
            if (stream.CanWrite)
            {
                string clientMessage = "This is a message from one of your clients.";
                // Convert string message to byte array.                 
                byte[] clientMessageAsByteArray = Encoding.ASCII.GetBytes(clientMessage);
                // Write byte array to socketConnection stream.                 
                stream.Write(clientMessageAsByteArray, 0, clientMessageAsByteArray.Length);
                Debug.Log("Client sent his message - should be received by server");
            }
        }
        catch (SocketException socketException)
        {
            Debug.Log("Socket exception: " + socketException);
        }
    }
}
